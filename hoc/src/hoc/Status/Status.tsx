import { useState, FC} from "react";
import { StatusProps } from "./interfaces";

export const Status = (WrappedComponent: FC<StatusProps>) => {
  const Component = () => {
    const [isError, setIsError] = useState(false);
    const [isLoading, setIsLoading] = useState(false);

    return (
      <div>
        <WrappedComponent
          isLoading={isLoading}
          isError={isError}
          setIsError={setIsError}
          setIsLoading={setIsLoading}
        />

        <button
          type="button"
          onClick={() => setIsError(prev => !prev)}
        >
          toggle error
        </button>

        <button
          type="button"
          onClick={() => setIsLoading(prev => !prev)}
        >
          toggle loading
        </button>
        {isError && <h5>error...</h5>}
        {isLoading && <h5>loading...</h5>}
      </div>
    )
  }

  return Component;
}
