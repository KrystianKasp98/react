import { FC } from "react";
import { Status } from "../hoc/Status/Status";
import { StatusProps } from "../hoc/Status/interfaces";

const GreetingComponent: FC<StatusProps> = ({
  isLoading,
  isError
}) => {

  return (
    <div>
      <h1>
        Hi all
      </h1>

      <h3>
        Error: {`${isError}`}
      </h3>

      <h3>
        Loading: {`${isLoading}`}
      </h3>

    </div>
  );
};

export const Greeting = Status(GreetingComponent);
